# Matinée du 1er Décembre 2020

## Découverte des WebSockets avec `socket.io`

### Le protocole web socket

Le protocole web socket (tel que décrit dans la [RFC 6455](https://datatracker.ietf.org/doc/rfc6455/?include_text=1)) permet d'établir une session de communication bilatérale entre un serveur [TCP](https://fr.wikipedia.org/wiki/Transmission_Control_Protocol) et un client web socket, comme un navigateur web par exemple.

La téchnologie WebSocket en résultant représente donc une alternative à la communication via HTTP, et permet un échange de données continu plus performant qu'à l'accoutumée.

Entre autres, une communication via WebSocket permet un échagne de données en binaire au lieu de texte, et n'oblige aucune réponse à chaque envoi de données comme le nécessite HTTP.

Imaginons une architecture client-serveur classique utilisant HTTP pour son transfert de données. Tout échange se déroulerait de la façon suivante.

```
┏━━━━━━━━━━┓                          ┏━━━━━━━━━━┓
┃          ┃                          ┃          ┃
┃          ┃          Requête         ┃          ┃
┃          ┠─────────────<────────────┨          ┃
┃          ┃                          ┃          ┃
┃          ┃          Réponse         ┃          ┃
┃          ┠─────────────>────────────┨          ┃
┃ SERVEUR  ┃                          ┃  CLIENT  ┃
┃          ┃          Requête         ┃          ┃
┃          ┠─────────────<────────────┨          ┃
┃          ┃                          ┃          ┃
┃          ┃          Réponse         ┃          ┃
┃          ┠─────────────>────────────┨          ┃
┃          ┃                          ┃          ┃
┗━━━━━━━━━━┛                          ┗━━━━━━━━━━┛
```

Dans le cas d'une communication via WebSocket cependant :

```
┏━━━━━━━━━━┓                          ┏━━━━━━━━━━┓
┃          ┃                          ┃          ┃
┃          ┃   Demande de connexion   ┃          ┃
┃          ┠────────────<─────────────┨          ┃
┃          ┃                          ┃          ┃
┃          ┃           OK             ┃          ┃
┃          ┠────────────>─────────────┨          ┃
┃          ┃                          ┃          ┃
┃          ┃        Messages          ┃          ┃
┃          ┠────────────<─────────────┨          ┃
┃          ┠────────────>─────────────┨          ┃
┃ SERVEUR  ┠────────────>─────────────┨  CLIENT  ┃
┃          ┠────────────<─────────────┨          ┃
┃          ┠────────────<─────────────┨          ┃
┃          ┠────────────<─────────────┨          ┃
┃          ┠────────────>─────────────┨          ┃
┃          ┠────────────>─────────────┨          ┃
┃          ┃                          ┃          ┃
┃          ┃                          ┃          ┃
┃          ┃  Fermeture de connexion  ┃          ┃
┃          ┠───────────<─>────────────┨          ┃
┃          ┃                          ┃          ┃
┗━━━━━━━━━━┛                          ┗━━━━━━━━━━┛
```

Dans le cas de HTTP, une requête demande une réponse, alors que dans le cas des websocket les données peuvent naviguer librement sous la forme de _paquets_ entre le client et le serveur, sans necessité de réponse, ce qui rend l'envoi de données récurrent foncièrement plus performant.

Le client ou le serveur peuvent demander la fermeture de la connexion de façon explicite, sinon la connexion se fermera automatiquement si aucune réponse n'intervient lors de messages ou lors d'envoi de paquet _ping_ (auquel il faut répondre par un paquet _pong_).

#### Quand utiliser HTTP et quand utiliser des WebSocket ?

Chaque solution a ses avantages et ses inconvénients bien évidemment.

Pour **HTTP** l'iconvénient réside dans sa _"lourdeur"_. C'est à dire qu'il faut beaucoup de courbettes et d'en-têtes et de paramètres envoyés à chaque requête pour respecter le protocole, et cela rajoute essentiellement des _frais de données_ (_overhead_ en anglais), ce qui est parfait pour des **échanges ponctuels**, mais qui le rend moins bon pour des transferts rapides et récurrents.

**WebSocket** cependant est très bon pour ces **nombreux transferts rapides et récurrents**, mais est moins utile pour des transferts de données ponctuels avec de nombreux clients. Chaque session de websocket ouverte est _plus gourmande_ en performances et chaque nouvelle connexion demande à effectuer une _poignée de mains_ (_handshake_ en anglais) qui se trouve être une requête HTTP elle même.

Du coup pour un seul transfert de données on se retrouverait à faire une requête HTTP "lourde", plus le transfert des données, alors qu'on aurait juste pu faire la requête elle même pour transferer les données.

Il existe bien d'autres avantages et inconvénients pour chacun, mais je ne vais pas être exhaustif pour ne pas trop rallonger cette explication, le principe est déjà clair je pense.

### Ouvrir un serveur de WebSocket avec `socket.io`

[socket.io](https://socket.io) est un framework permettant de faciliter la communication client/serveur à l'aide de WebSockets. Il fournit un client utilisant l'implémentation des WebSockets déjà disponible dans l'api des navigateurs web, et en facilite l'utilisation, et propose surtout un serveur TCP web socket facile à utiliser.

Il a besoin d'un serveur HTTP pour fonctionner (pour envoyer la requête d'ouverture de connexion entre autres) et on va donc l'utiliser en tandem avec `express`.

#### Express

`ExpressJs` est un framework web pour `nodejs` permettant de faciliter le développement d'un serveur web/http en fournissant du _middleware_ paramétrable.

Le framework facilite le paramétrage d'_endpoints_ et le service de données, et est donc très efficace pour mettre en place des API et des serveurs web statiques simples.

### Installation des dépendances

On les installe via `npm` grâce à

```
npm instal --save express socket.io
```

Une fois que c'est fait, on se crée un fichier qui accueillera les instructions de notre serveur, dans lequel on met en place nos serveurs http et WebSocket tel qu'indiqué dans la [documentation](https://socket.io/docs/v3).

On ajoute également un moyen pour notre serveur de websockets de réagir à une connexion client :

```js
//server.js
//on importe express
const express = require("express");
//on crée une application express permettant de mettre en place le serveur
const app = express();
//on récupère le serveur http node
const http = require("http");
//on passe l'application express au serveur pour lui servir de middleware
const server = http.createServer(app);
//on greffe ensuite socket.io a notre serveur
const io = require("socket.io")(server);

//process.env permet de récupérer des variables d'environnement lorsqu'on utilise node
//ces varibles d'environnement peuvent être précisées lors de l'appel du server en console ou dans un fichier d'environnement .env
const port = process.env.PORT || 4000;
server.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

//pour servir tous les fichiers d'un dossier de façon statique, on peut utiliser express.static à la place
//cela nous permettra de servir notre dossier client, avec notre application client de chat, de façon statique
app.use("/", express.static(__dirname + "/client"));

//on permet ensuite notre serveur de socket  de réagir à la connexion
io.on("connection", (socket) => {
    console.log(`${socket.id} connected`);
});
```

En lançant notre serveur avec `node server` on voit donc que notre serveur http est prêt, mais aucun message de notre serveur de WebSockets. Ce qui est normal, pour l'instant il n'affichera un message que lors d'une connexion client.

#### `io.on` et `socket` :

La formule `.on` fournie par notre serveur de WebSockets est semblable à un `EventListener`. Le serveur de WebSockets `io` attends donc un message intitulé `"connection"` et, à son arrivée, exécute le _callback_ qui se charge d'afficher un message dans la console.

L'objet `socket` qu'on utilise dans notre _callback_ est fourni par le serveur et représente la connexion ouverte avec le client. Celle ci nous permettra de discuter avec le(s) client(s) connecté(s), et possède un identifiant unique.

#### Implémentation d'une connexion client

Au tour de notre client d'être codé. On va en faire une application simple en html/js qui sera _servie_ par le serveur que l'on vient d'écrire.

Dans un dossier `client` on crée deux fichiers `index.html` et `main.js` :

En demandant le script à l'adresse `/socket.io/socket.io.js`, le serveur `socket.io` renverra une version de `socket.io` faite pour le _client_.

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Mini Chat</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <script src="/socket.io/socket.io.js"></script>
        <script src="main.js"></script>
    </head>
    <body>
        <h1>Mini Chat</h1>

        <div id="chat">
            <ul id="messages"></ul>
        </div>
        <form id="messageForm">
            <input type="text" id="messageInput" />
            <button>send</button>
        </form>
    </body>
</html>
```

On peut ensuite implémenter réellement le script se basant sur `socket.io.js` :

```js
document.addEventListener("DOMContentLoaded", () => {
    const socket = io();
});
```

# ECRITURE EN COURS, PAS FINI, BESOIN DU CODE

On décide d'implémenter quelque chose de très simple : un chat en temps réel.
Il nous faut donc un moyen d'envoyer des messages et de les relayer à tous les potentiels clients qui seraient connecté sur notre chat.

Si `io.on` permet de réagir à un évènement se passant au niveau du _pool_ de sockets _(de l'anglais pool pour piscine, désigne en gros un groupement d'objets)_, `socket.on` permet quant à lui de réagir à un évènement se passant sur une _connexion avec un client_ en particulier.

Ici à chaque paquet intitulé `new-message` reçu sur une connexion, on affiche ledit message dans notre console.

Si on lance notre serveur et notre application front-end (ou plusieurs applications front-end) et qu'on s'amuse à écrire dans le chat on voit effectivement les données arriver en temps réel dans notre console.

Génial, maintenant il faudrait que les messages s'affichent du côté du client et on serait les _rois du pétrôle_.

Notre serveur va donc émettre ce message à _tous les clients connectés_ pour que tout le monde puisse obtenir le dernier message en date :

Et il faut que nos clients puissent réceptionner ce `new-message` à leur tour et l'afficher dans leur interface !

#### Que fait notre application ?

On peut schématiser notre application de la façon suivante :

```
                    ┏━━━━━━━━━━━┓
  1- C → S          ┃           ┃
    "Coucou"        ┃           ┃
  ┏━━━━━━━━━━━━━━━━━╉╮ SERVEUR  ┃
  ┃                 ┃┊          ┃
  ┃                 ┃╰┄┄┄┄╮     ┃
  ┃                 ┗━━━━━┿━━━━━┛
  ┃                  2- S → C
  ↑   ╭────────────────"Coucou"─────────────────╮
  ┃   │                   │                     │
  ┃   ↓                   ↓                     ↓
  ┃   │                   │                     │
┏─┸───┴─────┓       ┏─────┴─────┓         ┏─────┴─────┓
│           │       │           │         │           │
│           │       │           │         │           │
│  CLIENT1  │       │  CLIENT2  │         │  CLIENT3  │
│           │       │           │         │           │
│           │       │           │         │           │
┗───────────┛       ┗───────────┛         ┗───────────┛
```

Très simple donc, mais efficace !

### Que faire ensuite ?

En cours on est allés un peu plus loin (à voir dans le code de l'application) en mettant en place un historique des messages pour que recharger la page n'efface pas tous les messages reçus, ainsi que l'affichage de l'heure d'envoi et du nom de l'auteur.

Tout ça se fait avec ce même principe de _réaction_ à la récéption de paquets.

Les applications possibles de cette communication en temps réel serait par exemple l'implémentation de jeux, transfert de données de sondes ou composants physiques pour une lecture en temps réel, détection de perte de connexion au client ([ping-pong](https://developer.mozilla.org/fr/docs/Web/API/WebSockets_API/Writing_WebSocket_servers#Pings-Pongs_le_coeur_des_WebSockets)), etc...
