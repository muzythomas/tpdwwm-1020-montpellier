# Démarrage d'un projet Angular

[voir la documentation officielle d'Angular](https://angular.io/guide/setup-local)

## Pré-requis

-   installer [nodejs](https://nodejs.org/en/)

## Installation d'Angular

Pour lancer un projet angular il faut d'abord installer `angular-cli` via `npm`.

```
    npm install -g @angular/cli
```

`install` permet d'installer une dépendance, `-g` permet de préciser que l'installation se doit d'être globale (comprendre: utilisable sur l'entiereté du système et pas juste le dossier courant)

#### note:

Pour lancer une commande venant d'une source externe depuis `powershell`, il faut autoriser les sources externes signées au niveau du système. Dans une fenêtre powershell ouvertes en tant qu'**administrateur** lancer la commande suivante :

```powershell
    Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
```

Une fois `angular-cli` installé, la commande `ng` devient accesible.
`ng` permet d'accéder à plusieurs outils de gestion de projets angular.
