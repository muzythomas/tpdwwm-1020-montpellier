# Authentification via JWT (Json Web Token)

## Qu'est ce que le JWT ?

JWT est un standard permettant l'échange sécurisé de jetons (tokens) entre plusieurs parties d'une application. Cette sécurité se traduit par la vérification de l'intégrité des données à l'aide d'une signature numérique, souvent RSA.

JWT dans le contexte de notre application nous servira à implémenter un système d'authentification via notre API en renvoyant un token en échange d'un couple nom d'utilisateur/mot de passe valide, en json.

## Schema de notre système d'auth

![schema jwt](./schema_jwt.PNG)

1. _Authentication_ : on envoie un couple username/password et on reçoit un `token`
2. _Authorization_ : on utilise ce `token` pour "_montrer patte blanche_" lors d'opérations demandant une autorisation particulière.

## Mise en place du JWT dans Symfony

Pour pouvoir utiliser ce système d'authentification via JWT, on utilise le bundle `lexik/jwt-authentication` :

```
composer require lexik/jwt-authentication-bundle
```

Celui ci génère un fichier `config/packages/lexik_jwt_authentication.yaml`, et modifie également notre .env.

Pour installer ce bundle et le paramétrer, on suit la [documentation](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#getting-started).

### Génération de clés SSH

Pour pouvoir signer en [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) les JWT, le bundle a besoin d'une clé privée et d'une clé publique.

Pour en générer, on a besoin d'une implémentation de SSL, ou OpenSSL sur notre ordinateur.
Si git pour windows est déjà installé, on peut utiliser l'executable d'openssl que git utilise pour générer nos clés.

On ajoute donc dans notre variable d'environnement système `PATH` le chemin suivant :

```
C:\Program Files\git\usr\bin
```

Avec ce chemin ajouté au path, on peut désormais utiliser la commande `openssl` dans notre terminal.

Pour générer des clés SSH dans notre projet, on se réfère donc aux commandes données dans la documentation :

Pour créer un dossier visant à contenir nos clés :

```
mkdir -p config/jwt
```

Pour générer la clé privée servant à déchiffrer :
(Lors de la génération de la clé privée, un mot de passe sera demandé, notez le quelque part !)

```
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
```

Pour générer la clé publique à partir de notre clé privée :

```
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

Les deux fichiers générés, `private.pem` et `public.pem` sont ensuite référencés dans le .env :

```
###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=VOTRE_MOT_DE_PASSE
###< lexik/jwt-authentication-bundle ###
```

### Paramétrage du module Security de Symfony

Une fois le lexik-jwt-authentication bundle installé, il faut paramétrer son fonctionnement dans le fichier `config/packages/security.yaml` :

```yaml
security:
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        #On prépare une règle de firewall pour notre page de login
        login:
            pattern: ^/login
            stateless: true
            anonymous: true
            json_login:
                #cette route est définie dans le config/routes.yaml
                check_path: /login_check
                #ces handlers sont ceux proposés par le bundle
                success_handler: lexik_jwt_authentication.handler.authentication_success
                failure_handler: lexik_jwt_authentication.handler.authentication_failure

        main:
            anonymous: lazy
            provider: app_user_provider

        #On prépare également une règle de firewall pour notre api
        api:
            pattern: ^/
            stateless: true
            guard:
                authenticators:
                    #Ici on indique que l'authentification doit se faire à l'aide du bundle
                    - lexik_jwt_authentication.jwt_token_authenticator

    access_control:
        #Ces lignes permettent de définir quelle partie du site demande de s'authentifier via le bundle, et quelle partie du site n'en a pas besoin
        #par exemple, pour /login, /register, ou /genre en méthode GET on ne demande pas à être déjà connecté
        - { path: ^/genre, roles: IS_AUTHENTICATED_ANONYMOUSLY, methods:[GET, HEAD] }
        - { path: ^/(login|register), roles: IS_AUTHENTICATED_ANONYMOUSLY }
        #Pour le reste de l'api cependant, il faut être authentifié
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY }
```

Notre `config/routes.yaml` contient le paramètre de route `login_check` permettant de gérer notre authentication :

```yaml
login_check:
    path: /login_check
```

## Implémentation de la connexion sur Angular

Pour que notre application front-end puisse s'authentifier auprès de notre API, elle doit donc se procurer le JWT valide à la connexion, puis l'ajouter en en-tête `Authorization` à chaque requête HTTP qui suivra.

### Mettre en place un service d'authentification

Pour effectuer une requête de connexion sur `login_check`, on crée donc un service

```
ng g s auth
```

Et on met en place une requête HTTP post dans ce service vers notre route login_check`

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class AuthService {
    constructor(private http: HttpClient) {}

    private loginUrl = "http://localhost:8000/login_check";

    public auth(username: string, password: string): Observable<any> {
        const body = {
            username: username,
            password: password,
        };
        return this.http.post(this.loginUrl, body);
    }

    public getToken() {
        const jwt = localStorage.getItem("jwt");
        return jwt;
    }
}
```

On appelle ensuite ce service dans un component pour pouvoir l'utiliser.

### Stockage du JWT dans le localStorage

Pour pouvoir insérer le JWT dans chacune des requêtes à suivre, on stocke, à la récéption du JWT, celui-ci dans le localStorage:

Par exemple, dans une méthode `auth()` d'un component :

```ts
auth(): void{
    this.authService.auth('test', 'test').subscribe(
            (res) => {
                const jwt = res.token;
                //on range le jwt dans le localstorage
                localStorage.setItem('jwt', jwt);
            },
            (err) => {
                console.error('wrong username or password');
            }
        );
}

```

### Utilisation du JWT dans toutes nos requêtes

Pour pouvoir ajouter `Authorization` comme en-tête de toutes les requêtes de notre application, au lieu d'ajouter chaque en-tête à chaque service, on utilise un [HttpInterceptor](https://angular.io/api/common/http/HttpInterceptor).

Un `HttpInterceptor` a pour rôle d'intercepter toutes les requêtes sortantes de notre application pour pouvoir appliquer un traitement supplémentaire avant de les rediriger vers leur destination originelle.

Pour mettre en place un `HttpInterceptor`, il faut donc créer une class qui implémente l'interface `HttpInterceptor`.

L'interface demande d'implémenter une méthode `intercept` qui possède comme paramètres la requête originelle `req: HttpRequest<any>` et un gestionnaire Http `next: HttpHandler`.

Le traitement à effectuer est très simple, il faut cloner la requête entrante et y ajouter le JWT dans un en-tête Authorization, si le JWT est trouvé dans le localStorage.

Sinon, on se contente de laisser passer la requête originelle sans modifications.

Une implémentation de ce traitement pourraît donc ressembler à ça :

```ts
import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    //un HTTPInterceptor sert à intercepter les requêtes sortantes de notre application
    //la méthode intercept obligatoire permet de définir ce qui doit arriver à la requête avant son départ
    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        //ici le rôle de notre authInterceptor est de vérifier la présence d'un jwt dans le localstorage, et si c'est le cas de le rajouter dans un en-tête Authorization avant d'envoyer la requête
        const jwt = this.authService.getToken();

        //si notre jwt a été récupéré
        if (jwt) {
            //on doit rajouter notre header Authorization
            //cependant, un objet HTTPRequest étant immuable, on ne peut y ajouter directement l'header à la volée, on doit cloner la requête
            const cloned = request.clone({
                headers: request.headers.append(
                    "Authorization",
                    `Bearer ${jwt}`
                ),
            });

            //on renvoie ensuite la requête clonée
            return next.handle(cloned);
        }
        //sinon on renvoie la requête originelle
        return next.handle(request);
    }
}
```

Une fois notre HttpInterceptor implémenté, il faut ensuite l'enregistrer comme Provider de notre module dans le `app.module.ts` :

```ts
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "./services/authInterceptor";

@NgModule({
    declarations: [
        AppComponent,
        //reste des declarations...
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        //reste des imports...
    ],
    providers: [
        //on enregistre notre AuthInterceptor comme HTTP_INTERCEPTOR de l'application
        //multi: true permet d'utiliser de multiples intercepteurs (dont celui par défaut d'angular) sans risque de conflits
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
```

[Plus d'informations sur les JWT](https://jwt.io/)
