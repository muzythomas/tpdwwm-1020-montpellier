import logo from "./logo.svg";
import "./App.css";
import React from "react";
import Task from "./Task";
import TaskForm from "./TaskForm";

function App() {
    const [tasks, setTasks] = React.useState([
        { text: "Faire la vaisselle", priority: "4" },
        { text: "Ranger l'appart", priority: "3" },
        { text: "Conquérir le monde", priority: "2" },
    ]);

    const addTask = (task) => {
        const newTasks = [...tasks, task];
        setTasks(newTasks);
    };

    return (
        <div>
            <TaskForm addTask={addTask} />
            <div className="todolist">
                {tasks.map((task, index) => (
                    <Task task={task} key="index" />
                ))}
            </div>
        </div>
    );
}

export default App;
