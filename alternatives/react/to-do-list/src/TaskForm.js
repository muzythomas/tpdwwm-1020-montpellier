import React from "react";

function TaskForm({ addTask }) {
    const [taskText, setTaskText] = React.useState("");
    const [taskPriority, setTaskPriority] = React.useState(0);

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(taskText, taskPriority);
        addTask({ text: taskText, priority: taskPriority });
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={taskText}
                onChange={(e) => setTaskText(e.target.value)}
            />
            <input
                value={taskPriority}
                onChange={(e) => setTaskPriority(e.target.value)}
                type="range"
                min="0"
                max="10"
                step="1"
            />

            <button type="submit">go</button>
        </form>
    );
}

export default TaskForm;
