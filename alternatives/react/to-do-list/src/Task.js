import React from "react";
import "./Task.css";
function Task({ task }) {
    return (
        <div className="task">
            {task.text} <span>{task.priority}</span>
        </div>
    );
}

export default Task;
