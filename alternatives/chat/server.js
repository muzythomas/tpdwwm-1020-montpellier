const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const io = require("socket.io")(server);

//process.env permet de récupérer des variables d'environnement lorsqu'on utilise node
//ces varibles d'environnement peuvent être précisées lors de l'appel du server en console ou dans un fichier d'environnement .env
const port = process.env.PORT || 4000;
server.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

//pour envoyer une réponse lors d'une requête sur une url, on peut utiliser notre app express
// app.get("/", (req, res) => {
//     res.sendFile(__dirname + "/client/index.html");
// });
//pour servir tous les fichiers d'un dossier de façon statique, on peut utiliser express.static à la place

app.use("/", express.static(__dirname + "/client"));

const history = [];

io.on("connection", (socket) => {
    console.log(`connected on ${socket.id}`);

    socket.emit("history", history);

    socket.on("disconnect", () => {
        console.log(`${socket.id} disconnected `);
        io.emit("userDisconnect", `${socket.id} disconnected`);
    });

    socket.on("newMessage", (message) => {
        console.log(`${socket.id} : ${message}`);
        const newMessage = {
            text: message,
            id: socket.id,
        };
        history.push(newMessage);
        io.emit("newMessage", newMessage);
    });
});
