document.addEventListener("DOMContentLoaded", () => {
    const socket = io();

    const messageInput = document.getElementById("messageInput");
    const messageForm = document.getElementById("messageForm");

    messageForm.addEventListener("submit", (event) => {
        //on empêche le formulaire de changer la page
        event.preventDefault();
        if (messageInput.value) {
            socket.emit("newMessage", messageInput.value);
            messageInput.value = "";
        }
    });

    const chatContainer = document.getElementById("chat");
    const messageContainer = document.getElementById("messages");

    socket.on("history", (history) => {
        history.forEach((message) => {
            displayMessage(message, messageContainer, socket.id);
        });
    });

    socket.on("newMessage", (message) => {
        displayMessage(message, messageContainer, socket.id);
    });

    socket.on("userDisconnect", (message) => {
        displayMessage(message, messageContainer, socket.id);
    });
});

function displayMessage(message, container, id) {
    const messageItem = document.createElement("li");
    messageItem.textContent = `${message.id} : ${message.text}`;

    if (message.id === id) {
        messageItem.classList.add("my-message");
    }
    container.appendChild(messageItem);
}
