document.addEventListener("DOMContentLoaded", () => {
    const layerOne = document.querySelector(".layer1");
    const layerTwo = document.querySelector(".layer2");
    const layerThree = document.querySelector(".layer3");
    const layerFour = document.querySelector(".layer4");

    layerOne.addEventListener("mousemove", (event) => {
        const pageX = event.clientX - window.innerWidth / 2;
        const pageY = event.clientY - window.innerHeight / 2;

        const fourRatio = -100;
        const threeRatio = -300;
        const twoRatio = -500;

        parallaxTranslate(layerFour, pageX, pageY, fourRatio);
        parallaxTranslate(layerThree, pageX, pageY, threeRatio);
        parallaxTranslate(layerTwo, pageX, pageY, twoRatio);
    });
});

function parallaxTranslate(elem, x, y, ratio) {
    elem.style.transform = `translateX(${50 + x / ratio}%) translateY(${
        50 + y / ratio
    }%)`;
    console.log(elem.style.transform);
}
