const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);

const io = require("socket.io")(server);
const port = 4000;

app.use("/", express.static("client"));

server.listen(port, () => {
    console.log(`server listening on http://localhost:${port}`);
});

function randColor() {
    return `rgb(${Math.floor(Math.random() * 255)},${Math.floor(
        Math.random() * 255
    )},${Math.floor(Math.random() * 255)})`;
}

let players = [];
io.on("connection", (socket) => {
    console.log(`${socket.id} connected`);

    players.push({
        x: 10,
        y: 10,
        color: randColor(),
        id: socket.id,
    });
    socket.emit("player-info", players[players.length - 1]);

    socket.on("request-update", (player) => {
        const found = players.find((p) => p.id === player.id);
        if (found) {
            found.x = player.x;
            found.y = player.y;
        }
        //console.log(players);
        const filteredPlayers = players.filter((p) => p.id != player.id);
        socket.emit("request-update", filteredPlayers);
    });

    socket.on("disconnect", () => {
        players = players.filter((p) => p.id != socket.id);
        io.emit("request-update", players);
    });
});
