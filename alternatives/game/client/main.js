document.addEventListener("DOMContentLoaded", () => {
    const socket = io();

    const gameCanvas = document.getElementById("gameCanvas");

    socket.on("player-info", (newPlayer) => {
        const player = new Player(
            newPlayer.x,
            newPlayer.y,
            newPlayer.color,
            newPlayer.id
        );

        const game = new Game(gameCanvas);
        game.players.push(player);

        socket.on("request-update", (updatedPlayers) => {
            game.players = [game.players[0], ...updatedPlayers];
        });

        const step = () => {
            socket.emit("request-update", game.players[0]);
            game.draw();
            requestAnimationFrame(step);
        };
        requestAnimationFrame(step);

        document.addEventListener("keydown", (event) => {
            console.log(game.players);
            switch (event.key) {
                case "ArrowUp": {
                    game.players[0].move(DIRECTIONS.UP);
                    break;
                }
                case "ArrowRight": {
                    game.players[0].move(DIRECTIONS.RIGHT);
                    break;
                }
                case "ArrowDown": {
                    game.players[0].move(DIRECTIONS.DOWN);
                    break;
                }
                case "ArrowLeft": {
                    game.players[0].move(DIRECTIONS.LEFT);
                    break;
                }
            }
        });
    });
});

const DIRECTIONS = {
    UP: 0,
    RIGHT: 1,
    DOWN: 2,
    LEFT: 3,
};
class Player {
    constructor(x, y, color, id) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.id = id;
    }

    move(direction) {
        switch (direction) {
            case DIRECTIONS.UP: {
                this.y -= 10;
                break;
            }
            case DIRECTIONS.RIGHT: {
                this.x += 10;
                break;
            }
            case DIRECTIONS.DOWN: {
                this.y += 10;
                break;
            }
            case DIRECTIONS.LEFT: {
                this.x -= 10;
                break;
            }
        }
    }
}

class Game {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");
        this.players = [];
    }

    draw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.players.forEach((player) => {
            this.ctx.fillStyle = player.color;
            this.ctx.fillRect(player.x, player.y, 50, 50);
        });
    }
}
