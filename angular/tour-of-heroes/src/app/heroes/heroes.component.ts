import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
    selector: 'app-heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.css'],
})
export class HeroesComponent implements OnInit {
    heroes: Hero[] = [];

    //pour utiliser l'injection de dépendance dans un component on peut passer nos services en paramètres du constructeur
    constructor(private heroService: HeroService) {}

    //ngOnInit() est ce qu'on appelle un lifeCycle Hook
    //c'est une méthode qui s'exécute à un certain moment du cycle de vie du component
    //ce cycle de vie représente chaque étape de la construction à la destruction d'un component dans l'application
    //ngOnInit se lance après la création du component avec le constructeur, et lorsque les données sont prêtes à être utilisées
    ngOnInit(): void {
        this.getHeroes();
    }

    getHeroes(): void {
        //heroService nous renvoie désormais un observable
        //ressemblant à une Promesse, on se doit de lui proposer une action à réaliser lors de la récéption de la donnée
        //pour ça, on utilise subscribe (souscrire), qui exécutera la fonction de callback lorsque la donnée nous parviendra
        //c'est nécessaire pour la gestion asynchrone de nos données
        this.heroService
            .getHeroes()
            .subscribe((heroesData) => (this.heroes = heroesData));
    }

    add(name: string, rating: number): void {
        if (!name) {
            return;
        }
        const hero = {
            name: name,
            rating: rating,
        };
        this.heroService
            .add(hero as Hero)
            .subscribe((hero) => this.heroes.push(hero));
    }

    delete(hero: Hero): void {
        this.heroService
            .delete(hero)
            .subscribe(
                () => (this.heroes = this.heroes.filter((h) => h !== hero))
            );
    }
}
