import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Dr Nice', rating: 9 },
    { id: 12, name: 'Narco', rating: 5 },
    { id: 13, name: 'Bombasto', rating: 3 },
    { id: 14, name: 'Celeritas', rating: 7 },
    { id: 15, name: 'Magneta', rating: 4 },
    { id: 16, name: 'RubberMan', rating: 5 },
    { id: 17, name: 'Dynama', rating: 2 },
    { id: 18, name: 'Dr IQ', rating: 9 },
    { id: 19, name: 'Magma', rating: 8 },
    { id: 20, name: 'Tornado', rating: 6 },
];
