import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css'],
})
export class MessagesComponent implements OnInit {
    //pour que notre messageService soit accessible en l'état dans notre template
    //il faut que celui-ci soit public
    //en effet, chaque donnée représentée dans notre component doit être publique pour être utilisée dans notre template
    //le template n'a pas la possibilité de voir les propriétés/méthodes privées
    constructor(public messageService: MessageService) {}

    ngOnInit(): void {}

    clear() {
        this.messageService.clear();
    }
}
