import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
    //Un Subject est un Observable dans lequel on peut envoyer des données qui seront distribuées à plusieurs observateurs
    //On peut ainsi y entrer les données de notre champ de recherche comme dans un observable mais qui serait également une source d'observables
    private searchTerms = new Subject<string>();

    //par convention on rajoute un $ a la fin d'un nom de variable contenant un observable
    heroes$: Observable<Hero[]>;

    constructor(private heroService: HeroService) {}

    ngOnInit(): void {
        //searchTerms étant un observable d'observables on peut jeter un oeil a ces observables via pipe()
        this.heroes$ = this.searchTerms.pipe(
            //debounceTime() permet de mettre en place un minuteur ne laissant passer les données qu'après un certain délai
            debounceTime(200),
            //distinctUntilChanged permet de ne pas considérer la dernière donnée en date si elle est identique à la précédente
            distinctUntilChanged(),
            //switchMap se comporte comme une map mais ne considère que le dernier observable récupéré, ce qui permet de renvoyer un observable au lieu d'un observable<observable>, et également de se débarasser des valeurs obsolètes
            switchMap((term: string) => this.heroService.searchHeroes(term))
        );

        //grâce à nos opérateurs on a ainsi un champ de recherche plus optimisé qui n'envoie les requêtes au service qu'une fois que la donnée est prête et risque moins de changer
    }

    search(term: string): void {
        this.searchTerms.next(term);
    }
}
