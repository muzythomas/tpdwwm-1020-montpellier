import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { GenreDetailComponent } from './genre/genre-detail/genre-detail.component';
import { GenreListComponent } from './genre/genre-list/genre-list.component';

const routes: Routes = [
    { path: 'genre', component: GenreListComponent },
    { path: 'genre/:id', component: GenreDetailComponent },
    { path: 'login', component: AuthComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
