import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Genre } from 'src/app/model/genre';
import { GenreService } from 'src/app/service/genre.service';

@Component({
    selector: 'app-genre-detail',
    templateUrl: './genre-detail.component.html',
    styleUrls: ['./genre-detail.component.css'],
})
export class GenreDetailComponent implements OnInit {
    genre?: Genre;
    formUpdate?: FormGroup;
    editMode: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private gs: GenreService,
        private fb: FormBuilder
    ) {}

    ngOnInit(): void {
        this.route.paramMap.subscribe((paramMap) => {
            const id = +paramMap.get('id')!;
            this.gs.get(id).subscribe((data) => {
                this.genre = data;
                this.formUpdate = this.fb.group({
                    name: [this.genre.name],
                });
            });
        });
    }

    update() {
        //étant donné que update() ne sera appelée qu'à l'envoi du formulaire, on peut affirmer  que notre formUpdate sera bien présent (!)
        const genre: Genre = this.formUpdate!.value;
        genre.id = this.genre!.id;
        //on sort du mode d'édition
        //bug sous chrome : this.toggleEdit();
        //sous chrome le focusout et la suppression (removeChild) de l'élément <form> surviennent au même moment, entrainant un bug dans angular
        this.gs.update(genre).subscribe((data) => {
            this.genre = data;
        });
    }

    toggleEdit() {
        this.editMode = !this.editMode;
    }
}
