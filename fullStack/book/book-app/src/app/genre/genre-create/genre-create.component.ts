import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Genre } from 'src/app/model/genre';
import { GenreService } from 'src/app/service/genre.service';

@Component({
    selector: 'app-genre-create',
    templateUrl: './genre-create.component.html',
    styleUrls: ['./genre-create.component.css'],
})
export class GenreCreateComponent implements OnInit {
    //@Output est l'inverse de @Input, en cela que ça nous permet de faire passer une information d'un enfant à un parent
    //Ici, cette donnée en @Output étant un évènement, le parent pourra y réagir avec (onCreate) placé dans la balise du composant
    //cela nous permet de réagir a notre eventEmitter comme on réagirait à (click) ou (ngSubmit) par exemple
    @Output() onCreate = new EventEmitter<Genre>();

    //FormBuilder est un service permettant la mise en place de formulaires dynamiques
    constructor(private fb: FormBuilder, private gs: GenreService) {}

    //Un FormGroup est un ensemble de FormControl
    //Un FormControl est un ensemble de paramètres de controles de formulaire visant un champ de formulaire
    createForm!: FormGroup;

    ngOnInit(): void {
        //pour créer un formulaire on peut par exemple initialiser un FormGroup via FormBuilder.group()
        this.createForm = this.fb.group(
            //dans ce FormGroup il faut ensuite définir chaque FormControl
            {
                //Le FormControl le plus simple possible est simplement un nom de champ (ici ce nom est 'name')
                //auquel on associe une valeur de remplissage (ici une chaine vide) et des paramètres de validation (un tableau)
                name: [
                    '',
                    [
                        Validators.required,
                        Validators.maxLength(30),
                        //Validators.pattern permet d'utiliser une Regex pour vérifier qu'un champ input corresponde bien à un format choisi
                        //c'est l'équivalent de l'attribut html "pattern" qu'on peut rajouter dans une balise
                        //la regex suivante : '/(?!^ +$)^.+$/' permet de s'assurer que notre formControl ne contienne pas QUE des espaces
                        //(?!^ +$) -- cette partie de la regex empêche de matcher des string uniquement composées d'espaces
                        //(?!) ce groupe là veut dire "non"
                        //^ +$  ce groupe là veut dire "que des espaces" -- ^ signifie le début de la phrase, $ signifie la fin, + signifie "un ou plusieurs"
                        //^.+$ -- cette partie là se charge de matcher les strings composées de caractères
                        //. signifie "n'importe quel caractère"
                        Validators.pattern('(?!^ +$)^.+$'),
                    ],
                ],
            }
        );
    }

    get formName() {
        return this.createForm.get('name')!;
    }

    create(): void {
        //pour récupérer les valeurs d'un formulaire on peut cibler le formgroup et récupérer sa value, de façon à obtenir un objet avec toutes les propriétés représentées par les formControl
        //on peut également aller chercher le formControl directement
        //Chaque valeur des couples clé valeur dans notre FormGroup représente une valeur de champ de formulaire
        const genre: Genre = this.createForm.value;
        //on retire les espaces avant et après
        genre.name = genre.name.trim();

        //on vide notre champ géré par le FormControl "name"
        this.createForm.get('name')!.reset();
        this.gs
            .create(genre)
            .subscribe((res) => this.onCreate.emit(res.created));
        //eventEmitter.emit permet d'envoyer un évènement, ici contenant notre donnée, qui pourra être capturé comme n'importe quel évènement
    }
}
