import { Component, OnInit } from '@angular/core';
import { Genre } from 'src/app/model/genre';
import { AuthService } from 'src/app/service/auth.service';
import { GenreService } from 'src/app/service/genre.service';

@Component({
    selector: 'app-genre-list',
    templateUrl: './genre-list.component.html',
    styleUrls: ['./genre-list.component.css'],
})
export class GenreListComponent implements OnInit {
    constructor(
        private genreService: GenreService,
        private authService: AuthService
    ) {}

    genres?: Genre[];

    ngOnInit(): void {
        this.getAll();
    }
    isLoggedIn() {
        return this.authService.getToken() !== null;
    }

    getAll(): void {
        this.genreService
            .getAll()
            .subscribe((genres: Genre[]) => (this.genres = genres));
    }

    created(genre: Genre) {
        this.genres?.push(genre);
    }

    delete(id: number) {
        //on met à jour la liste locale
        this.genres = this.genres!.filter((genre) => genre.id != id);
        this.genreService.delete(id).subscribe((res) => {
            console.log(res);
        });
    }
}
