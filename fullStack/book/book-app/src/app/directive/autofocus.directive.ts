import { AfterContentInit, Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[appAutofocus]',
})
export class AutofocusDirective implements AfterContentInit {
    //pour pouvoir récupérer une référence a notre élément visé par la directive, on peut demander une injection de cet élément envleoppé dans une ElementRef
    constructor(private el: ElementRef) {}

    ngAfterContentInit() {
        //afterContentInit est un hook de cycle de vie permettant d'exécuter du code après que le contenu de l'élément ciblé par la directive soit entièrement chargé
        this.el.nativeElement.focus();
    }
}
