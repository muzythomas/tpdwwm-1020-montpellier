import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    //un HTTPInterceptor sert à intercepter les requêtes sortantes de notre application
    //la méthode intercept obligatoire permet de définir ce qui doit arriver à la requête avant son départ
    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        //ici le rôle de notre authInterceptor est de vérifier la présence d'un jwt dans le localstorage, et si c'est le cas de le rajouter dans un en-tête Authorization avant d'envoyer la requête
        const jwt = this.authService.getToken();
        //si notre jwt a été récupéré
        if (jwt) {
            //on doit rajouter notre header Authorization
            //cependant, un objet HTTPRequest étant immuable, on ne peut y ajouter directement l'header à la volée, on doit cloner la requête
            const cloned = request.clone({
                headers: request.headers.append(
                    'Authorization',
                    `Bearer ${jwt.token}`
                ),
            });

            //on renvoie ensuite la requête clonée
            return next.handle(cloned);
        }
        //sinon on renvoie la requête originale
        return next.handle(request);
    }
}
