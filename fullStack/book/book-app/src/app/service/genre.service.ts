import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Genre } from '../model/genre';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class GenreService extends ResourceService<Genre> {
    constructor(http: HttpClient) {
        super(http, 'genre');
    }
}
