import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Created } from '../model/created';
import { Resource } from '../model/resource';

@Injectable({
    providedIn: 'root',
})
export class ResourceService<T extends Resource> {
    constructor(private http: HttpClient, private endpoint: string) {}

    private apiUrl: string = `http://localhost:8000/${this.endpoint}`;

    getAll(): Observable<T[]> {
        return this.http.get<T[]>(this.apiUrl);
    }

    create(resource: T): Observable<Created<T>> {
        return this.http.post<Created<T>>(this.apiUrl, resource);
    }

    get(id: number): Observable<T> {
        return this.http.get<T>(`${this.apiUrl}/${id}`);
    }

    update(resource: T): Observable<T> {
        return this.http.put<T>(`${this.apiUrl}/${resource.id}`, resource);
    }

    delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.apiUrl}/${id}`);
    }
}
