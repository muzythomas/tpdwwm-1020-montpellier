import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Format } from '../model/format';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class FormatService extends ResourceService<Format> {
    constructor(http: HttpClient) {
        super(http, 'format');
    }
}
