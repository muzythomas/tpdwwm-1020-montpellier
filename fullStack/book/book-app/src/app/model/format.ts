import { Resource } from './resource';

export interface Format extends Resource {
    id: number;
    name: string;
    width: number;
    height: number;
    book: any;
}
