import { Resource } from './resource';

export interface Genre extends Resource {
    id: number;
    name: string;
    books: any[];
}
