export interface Created<T> {
    created: T;
}
