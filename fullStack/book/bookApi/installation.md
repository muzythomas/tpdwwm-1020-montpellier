# Créer un projet d'API basique en Symfony

## 1 - Mise en place de Symfony

Pour créer un projet symfony destiné à une API Web, on utilise, comme pour un projet de site web, `symfony new`. Cependant, comme certaines des fonctionnalités (comme `twig` et les `templates`) ne nous serviront pas, le projet sera plus léger que son penchant application complète.

```
symfony new nomDuProjet
```

On se retrouve avec un projet symfony plus simple, et il nous faut donc installer certains des bundles nécessaires au développement de notre API.

Pour installer Doctrine et les fonctions ORM associées :

```
composer require symfony/orm-pack
```

Pour installer le Maker-Bundle nous aidant à la création de nos entités et nos migrations :

```
composer require --dev symfony/maker-bundle
```

Le `--dev` signifie que la dépendance du projet n'existe qu'en mode développement.

Pour utiliser les fonctions de `serialization` il nous faut installer également un composant `serializer` :

```
composer require symfony/serializer-pack
```

Pour que le `serializer` et d'autres composants de symfony puissent accéder facilement aux différentes propriétés de nos objets, les bundles `property-access` et `property-info` sont installé dans le `serializer-pack`.

Si on souhaite les installer séparement on peut faire

```
composer require symfony/serializer
```

et

```
composer require symfony/property-info symfony/property-access
```

PropertyInfo est un composant permettant de lire les propriétés d'un objet de façon plus "comfortable" sous forme de `string`.
PropertyAccess quant à lui permet d'accéder aux propriétés d'un objet à l'aide de `string`.

## Création des entités et migrations

Doctrine nécessite toujours d'avoir paramétré notre `.env` pour se connecter à la base de données.

Une fois le maker-bundle installé, on peut créer nos entités comme dans n'importe quel projet symfony avec

```
php bin/console make:entity
```

puis effectuer nos migrations avec

```
php bin/console make:migration
php bin/console doctrine:migrations:migrate
```

## Création d'un Controller et programmation de l'API

Pour programmer notre API, il faut désormais faire correspondre des routes et méthodes HTTP avec des actions sur notre base de données.
Pour pouvoir connecter du code à un route, comme dans tout projet symfony, il faut utiliser un `controller`.

```
php bin/console make:controller
```

Une fois le controller créé, on voit que le code généré diffère légèrement de notre projet symfony web classique, en celà que la réponse se fait au format `json`.

Il faut maintenant coder les différentes routes (`CRUD` par exemple, `C`reate `R`ead `U`pdate `D`elete), avec les différentes méthodes HTTP (`POST`, `GET`, `PUT`, `DELETE`).

### Gestion des Méthodes Acceptées par Route

Pour définir les méthodes acceptées pour une Route, et donc définir quelle route exécutera quel code, on peut définir dans l'annotation de la route le paramètre `methods` :

Par exemple, pour un `endpoint` qui listerait toutes nos ressources via une requête `GET` on pourrait définir :

```php
#[Route('/genre', name: 'genre', methods: ["GET", "HEAD"])]
public function list()
{}
```

Le paramètre

```
methods: ["GET", "HEAD"]
```

indique donc que seules les méthodes `GET` et `HEAD` déclencheront la fonction `list()` de notre controleur.

Notre implémentation de `list` doit ensuite donc renvoyer du `json` :

```php
#[Route('/genre', name: 'genre', methods: ["GET", "HEAD"])]
    public function list(GenreRepository $repository): Response
    {
        $genres = $repository->findAll();
        //$this->json() crée une JsonResponse renvoyant nos données au format JSON
        //la méthode se charge également d'appeler un serializer s'il existe pour convertir nos objets en JSON
        return $this->json($genres);
    }
```

Une fois notre requête `GET` envoyée sur notre serveur à l'uri `/genre` on reçoit donc du `json` représentant notre tableau de ressources.

## Le Serializer

Pour que Symfony puisse convertir du json en objets et vice versa, il fait appel à un composant du framework nommé le `Serializer`.
La `serialization` est le fait de transformer des objets en texte, et la `deserialization` est le fait de transformer du texte en objets.
En installant le `symfony/serializer` via composer, on offre à Symfony la possibilité d'appeler cette fonctionnalité, notamment lors de l'appel de `$this->json` dans notre controller.

On peut également appeler le `Serializer` en tant que service, par injection de dépendance, dans une méthode de controller.
Par exemple :

```php
#[Route('/genre', name: 'genre-create', methods:["POST"])]
public function create(Request $request, SerializerInterface $serializer)
{
    //getContent permet de récup le corps de notre requête
    $data = $request->getContent();
    //le serializer est un composant permettant de transformer du texte formatté en un objet et vice versa
    //en deserializant en un Genre, on transforme du json représentant une entité Genre en une entité Genre, manipulable par Doctrine
    $genre = $serializer->deserialize($data, Genre::class, 'json');

    //cette entité manipulable par Doctrine peut ensuite être enregistrée
    $this->getDoctrine()->getManager()->persist($genre);
    $this->getDoctrine()->getManager()->flush();

    //lorsque $this->json s'execute, il va tenter de serializer (objet -> json) ce qu'on lui donne
    //grace a notre serializer il peut transformer notre entité Genre en du json représentant un Genre
    return $this->json([
            'created' => $genre
    ]);
}
```

En demandant d'injecter un service correspondant à `SerializerInterface` on récupère notre service `Serializer` qui nous permet d'utiliser ses fonctionnalités.
Pour utiliser `deserialize` il faut préciser en quelle entité on veut convertir le corps de la requête (ici `Genre::class`), mais également en quel format est écrit le corps de la requête (ici `json`).

Si on veut deserializer des données dans une entité déjà existante (dans le cadre d'un `update` par exemple), on peut préciser une option `object_to_populate` à notre `serializer` pour lui indiquer quelle entité cibler.

```php
#[Route('/genre/{id}', name:"genre_update", methods:["PUT"])]
public function update(int $id, Request $request, GenreRepository $rep, SerializerInterface $serializer): Response
{
$genre = $rep->find($id);
if (!$genre){
    return $this->json([
        "message" => "Genre not found"
        ], 404);
    }
    $data = $request->getContent();
    //au moment de la deserialization de notre JSON, il faut qu'on le place dans notre entité déjà existante
    //pour pouvoir remplacer la ressource, on précise donc object_to_populate de façon à ce que le serializer range les données dans l'entité
    $serializer->deserialize($data, Genre::class, 'json', [
        'object_to_populate' => $genre
    ]);
    //il nous suffit ensuite de flush
    $this->getDoctrine()->getManager()->flush();
    return $this->json($genre);
}
```
