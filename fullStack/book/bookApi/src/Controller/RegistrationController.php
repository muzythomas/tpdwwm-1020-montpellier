<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'register', methods: ["POST"])]
    public function index(Request $request, SerializerInterface $serializer, UserPasswordEncoderInterface $encoder): Response
    {
        $data = $request->getContent();

        $user = $serializer->deserialize($data, User::class, 'json');
        $user->setPassword(
            $encoder->encodePassword(
                $user,
                $user->getPassword()
            )
        );

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->json([
            "message" => "user successfully created"
        ], 201);
    }
}
