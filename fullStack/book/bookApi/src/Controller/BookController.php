<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/book')]
class BookController extends AbstractController
{
    #[Route('', name: 'book', methods: ['GET', 'HEAD'])]
    public function list(BookRepository $br): Response
    {
        $books = $br->findAll();
        return $this->json($books, 200, [], ['groups' => 'book']);
    }

    #[Route('', name:'book-create', methods: ['POST'])]
    public function create(Request $request, SerializerInterface $serializer, EntityManagerInterface $em): Response 
    {
        $data = $request->getContent();

        $book = $serializer->deserialize($data, Book::class, 'json', [
            'groups' => 'book'
        ]);

        $em->persist($book);
        $em->flush();

        return $this->json($book, 201, [], ['groups' => 'book']);
    }
}
