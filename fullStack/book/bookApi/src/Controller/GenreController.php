<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Repository\GenreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/genre')]
class GenreController extends AbstractController
{
    //note : #[Route] est la nouvelle syntaxe PHP 8 pour les annotations qui est utilisée dans symfony pour les routes
    //Pour diriger nos requêtes selon leurs méthodes HTTP on peut préciser les méthodes dans l'annotationde route
    //ici, notre fonction index() ne sera appelée que si les méthodes HTTP GET ou HEAD sont utilisées pour effectuer la requête
    #[Route('', name: 'genre', methods: ["GET", "HEAD"])]
    public function list(GenreRepository $repository): Response
    {
        $genres = $repository->findAll();
        //$this->json() crée une JsonResponse renvoyant nos données au format JSON
        //la méthode se charge également d'appeler un serializer s'il existe pour convertir nos objets en JSON
        return $this->json($genres, 200, [], [
            "groups" => "genre"
        ]);
    }

    #[Route('/{id}', name: 'genre_view', methods: ["GET", "HEAD"])]
    public function view(int $id, GenreRepository $rep): Response
    {
        $genre = $rep->find($id);
        if (!$genre){
            return $this->json(
                [
                "message" => 'Genre not found'
                ], 
                404);
        }
        return $this->json($genre, 200, [], [
            "groups" => "genre"
        ]);
    }

    //pour la création d'une entité, nous avons besoin d'intercepter une requête avec une méthode POST, il faut donc préciser methods: ["POST"] pour indiquer au routeur de matcher cette route
    #[Route('', name: 'genre-create', methods:["POST"])]
    public function create(Request $request, SerializerInterface $serializer)
    {
        //getContent permet de récup le corps de notre requête
        $data = $request->getContent();
        //le serializer est un composant permettant de transformer du texte formatté en un objet et vice versa
        //en deserializant en un Genre, on transforme du json représentant une entité Genre en une entité Genre, manipulable par Doctrine
        $genre = $serializer->deserialize($data, Genre::class, 'json');

        //cette entité manipulable par Doctrine peut ensuite être enregistrée 
        $this->getDoctrine()->getManager()->persist($genre);
        $this->getDoctrine()->getManager()->flush();

        //lorsque $this->json s'execute, il va tenter de serializer (objet -> json) ce qu'on lui donne
        //grace a notre serializer il peut transformer notre entité Genre en du json représentant un Genre 
        return $this->json([
            'created' => $genre
        ], 201, [], ["groups" => "genre"]);
    }

    #[Route('/{id}', name: 'genre_delete', methods:["DELETE"])]
    public function delete(int $id, GenreRepository $rep, EntityManagerInterface $em): Response 
    {   
        $genre = $rep->find($id);
        if (!$genre){
            return $this->json([
                "message" => "Genre not found"
            ], 404);
        }
        $em->remove($genre);
        $em->flush();
        return $this->json([
            "message" => "Genre deleted successfully"
        ]);
    }

    //On utilise la méthode PUT pour effectuer une mise à jour
    //PUT permet de décrire un remplacement d'une ressource par une autre représentation
    #[Route('/{id}', name:"genre_update", methods:["PUT"])]
    public function update(int $id, Request $request, GenreRepository $rep, SerializerInterface $serializer): Response
    {
        $genre = $rep->find($id);
        if (!$genre){
            return $this->json([
                "message" => "Genre not found"
            ], 404);
        }
        $data = $request->getContent();

        //au moment de la deserialization de notre JSON, il faut qu'on le place dans notre entité déjà existante 
        //pour pouvoir remplacer la ressource, on précise donc object_to_populate de façon à ce que le serializer range les données dans l'entité 
        $serializer->deserialize($data, Genre::class, 'json', [
            'object_to_populate' => $genre
        ]);
        //il nous suffit ensuite de flush
        $this->getDoctrine()->getManager()->flush();
        return $this->json($genre, 200, [], [
            "groups" => "genre"
        ]);
    }
}
