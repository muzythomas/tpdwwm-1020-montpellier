<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController {


    /**
     * @Route("/index", name="article_list")
     */
    public function index(): Response{
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repository->findAll();
        dump($articles);
        return $this->render("index.html.twig", [
            "articles" => $articles
        ]);
    }

    /**
     * @Route("/create")
     */
    public function create(Request $request): Response {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $article->setCreatedAt(new \DateTime());
            $article->setAuthor($this->getUser());
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();
            
            return $this->redirectToRoute('article_list');
        }

        return $this->render('create.html.twig', [
            "articleForm" => $form->createView()
        ]);
    }

    /**
     * @Route("/category/{name}", name="articleByCategory")
     */
    public function articleByCategory(Category $category){
        return $this->render("index.html.twig", [
            "articles" => $category->getArticles()
        ]);
    }
}